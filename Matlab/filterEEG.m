function [output] = filterEEG(input)
    
    output = cell(1, size(input, 2));    % Output is cell array as input is.
    bandpassFilter = load('Bandpass.mat');  %Load designed filter
    for i=1:size(input, 2)
        array_tmp = cell2mat(input(i));
        trials_num = size(array_tmp, 3);    % save number of trials for each action
        data_tmp = zeros(size(array_tmp, 1), size(array_tmp, 2), size(array_tmp, 3));
        for j=1:trials_num
            data_tmp(:, :, j) = 24e-3* filter(bandpassFilter.bandpass, array_tmp(:, :, j));
        end
        output(i) = mat2cell(data_tmp, size(array_tmp, 1), 63, trials_num);
    end

end

