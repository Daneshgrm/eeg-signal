function output = dsample(input, rate)
    output = cell(1, size(input, 2));
    for i=1:size(input, 2)
        array_tmp = cell2mat(input(i));
        trial_nums = size(array_tmp, 3);
        d_data_tmp = zeros(7200/rate, 63, trial_nums);
        for j=1:trial_nums
            d_data_tmp(:, :, j) = downsample(array_tmp(1:63, :, j)', rate);
        end
        output(1, i) = mat2cell(d_data_tmp, 7200/rate, 63, trial_nums);
    end
end

