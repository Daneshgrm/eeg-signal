function [best_features, selected_features_indices] = jvalue(features, threshold)
    loop_matrix = ones(4);
    loop_matrix = triu(loop_matrix, 1);
    feature_matrix_tmp = cell2mat(features.feature_matrix);
    for i=1:3
        cls1_feature = cell2mat(features.sep_features(i));
        index_tmp = find(loop_matrix(i, :) == 1);
        for j=index_tmp
            cls2_feature = cell2mat(features.sep_features(j));
            go_mean = mean(feature_matrix_tmp(:, 1:end-1));
            lo_mean_cls1 = mean(cls1_feature(:,1:end-1));
            lo_var_cls1 = var(cls1_feature(:,1:end-1));
            lo_mean_cls2 = mean(cls2_feature(:,1:end-1));
            lo_var_cls2 = var(cls2_feature(:,1:end-1));
            J(j, :) = ((go_mean-lo_mean_cls1).^2 + (go_mean-lo_mean_cls2).^2)./(lo_var_cls1+lo_var_cls2+eps);
        end
        sum_J = sum(J);
        [~, index] = sort(sum_J, 'descend');
        selected_features_indices = index(1:threshold);
        selected_features = feature_matrix_tmp(:, selected_features_indices);
    end
    best_features = [selected_features feature_matrix_tmp(:, end)];
end
