function output = featureExtraction(input)
    trials_numbers = findTrialsNo(input);
    eeg_features = mainFeature(input);
    wave_features = waveFeature(input);
    features = [eeg_features; wave_features];
    output.sep_features = combineFeature(features);
    if(size(input, 2) > 1)
        output.feature_matrix = gen_feat_cell(output.sep_features, trials_numbers);
    else
        output.feature_matrix = output.sep_features;
    end
     function total_features = combineFeature(input)
        total_features = cell(1, 4);
        for i = 1:size(input, 2)
            input1 = cell2mat(input(1, i));
            input2 = cell2mat(input(2, i));
            array_tmp = [input1, input2, i*ones(size(input1,1), 1)];
            total_features(i) = mat2cell(array_tmp, size(array_tmp, 1), size(array_tmp, 2));
        end
    end
    function wavelet_feature_class = waveFeature(input)
    
        wavelet_feature_class = cell(1, size(input, 2));
        trials_nums = findTrialsNo(input);
        data_wavelets = waveletTrans(input);
        for i = 1:size(input, 2)
            trials_num = trials_nums(i+1);
            for j = 1:size(data_wavelets, 1)
                wave_tmp =cell2mat(data_wavelets(j, i));
                for k = 1:trials_num
                    
                    variance_f = var(wave_tmp(:, :, k));
                    mean_freq_f= meanfreq(wave_tmp(:, :, k), 100);
                    med_freq_f= medfreq(wave_tmp(:, :, k), 100);
                    
                    bandpower_f = bandpower(wave_tmp(:, :, k));
                    [max_pspectrum_f, max_pspectrum_freq_f] = pspectrum_f(wave_tmp(:, :, k));
                    entropy_f = wentropy_f(wave_tmp(:, :, k));

                    zcr_f = mean(abs(diff(sign(wave_tmp(:, :, k)))));
                    
                    if k==1
                        wave_feature = zeros(size(data_wavelets, 1), length([mean_freq_f med_freq_f bandpower_f max_pspectrum_f max_pspectrum_freq_f zcr_f variance_f entropy_f]), trials_num);  
                    end
                    wave_feature(j, :, k) = [mean_freq_f med_freq_f  ...
                          bandpower_f max_pspectrum_f max_pspectrum_freq_f zcr_f variance_f entropy_f];
                end
            end
            trial_features = zeros(1, size(wave_feature, 2)*3, size(wave_feature, 3));
            trial_features(1, :, :) = [wave_feature(1, :, :), wave_feature(2, :, :), wave_feature(3, :, :)]; 
            trial_features = shiftdim(trial_features)';
            wavelet_feature_class(i) = {trial_features};
        end
    end
    function feature_class_cell = mainFeature(input)
        feature_class_cell = cell(1, size(input, 2));
        trialsNo = findTrialsNo(input);
        for i=1:size(input, 2)
            data_tmp = cell2mat(input(i));
            trials_num = trialsNo(i+1);
            for j=1:trials_num
                variance_f = var(data_tmp(:, :, j));
                
                % Sine & Cosine Transform
                cosine_coeff = dct(data_tmp(:, :, j));
                cosine_coeff = cosine_coeff(:)';
%                 sine_coeff = dct(data_tmp(:, :, j));
%                 sine_coeff = sine_coeff(:)';
                skewness_f = skewness(data_tmp(:, :, j));
                % Fourier Transform
%                 fftmag_f = abs(fft(data_tmp(:, :, j)));
%                 fftmag_f = fftmag_f(:)';
                % FormFactor
                FormFactor_f = FF_f(data_tmp(:, :, j), variance_f);
                % Correlation
                corr_chanls = corr(data_tmp(:, :, j));
                corr_chanls = triu(corr_chanls);
                corr_chanls = corr_chanls(:)';
                index_tmp = corr_chanls ~= 0;
                corr_chanls = corr_chanls(index_tmp);
                 
                data_tmp(:, :, j) = normalize(data_tmp(:, :, j));
                % Histogram & Skewness
                hist_f = hist(data_tmp(:, :, j), 10);
                hist_f = hist_f(:)';
                if j==1
                    feature_tmp = zeros(1, length([cosine_coeff FormFactor_f skewness_f hist_f corr_chanls]), trials_num);  
                end
                feature_tmp(1, :, j) = [cosine_coeff FormFactor_f skewness_f hist_f corr_chanls ];
            end
            feature_trial = shiftdim(feature_tmp);
            feature_class = feature_trial';
            feature_class_cell(i) = {feature_class};
        end    
    end
    function trial_numbers = findTrialsNo(input)
           trial_numbers = zeros(1, size(input, 2)+2);
           for length_cnt = 1:size(input, 2)
               array_tmp = cell2mat(input(length_cnt));
               trial_numbers(length_cnt+1) = size(array_tmp, 3);
           end
           trial_numbers(size(input, 2)+2) = sum(trial_numbers(1:size(input, 2)+1)); 
    end
    function wavelets = waveletTrans(input)
            wavelets = cell(3, 4);
            for cls_cntr=1:size(input, 2)
                data_tmp = cell2mat(input(cls_cntr));
                data_size = size(data_tmp);
                [c, l] = wavedec(data_tmp(:, 1, 1), 3, 'db4');
                alpha = zeros(l(4), data_size(2), data_size(3));
                beta1 = zeros(l(3), data_size(2), data_size(3));
                beta2 = zeros(l(2), data_size(2), data_size(3));
                for bands_cntr=1:data_size(3)
                    for smpls_cntr=1:data_size(2)
                        [c_tmp, l_tmp] = wavedec(data_tmp(:, smpls_cntr, bands_cntr), 3, 'db4');
                        [alpha(:, smpls_cntr, bands_cntr), beta1(:, smpls_cntr, bands_cntr), beta2(:, smpls_cntr, bands_cntr)] = detcoef(c_tmp, l_tmp, [1 2 3]);
                    end
                end
                eeg_separated = [alpha; beta1; beta2];
                wavelets(:, cls_cntr) = mat2cell(eeg_separated, [l(4) l(3) l(2)], data_size(2), data_size(3));
            end
    end
    function outputff = FF_f(input, main_var)

            signal_o1d = diff(input);
            o1d_variance = var(signal_o1d);
            signal_o2d = diff(input, 2);
            o2d_variance = var(signal_o2d);
            outputff = (o2d_variance./o1d_variance)./(o1d_variance./main_var);

    end
    function [max_pspectrum, max_pspectrum_freq] = pspectrum_f(input)

            [pspectrum_tmp ,pspectrum_freq_tmp]= pspectrum(input, 100, 'FrequencyLimits', [7 35]);
            max_pspectrum = max(pow2db(pspectrum_tmp));
            [loc1_tmp, loc2_tmp] = find(max_pspectrum == pow2db(pspectrum_tmp));
            max_pspectrum_freq = pspectrum_freq_tmp(loc1_tmp)';

    end
    function outputent = wentropy_f(input_ent)
            E = zeros(2, size(input_ent, 2));
            for ie=1:size(input_ent, 2)
                E(1, ie) = wentropy(input_ent(:, ie), 'shannon');
                E(2, ie) = wentropy(input_ent(:, ie), 'log energy');
            end
            outputent = E(:)';
    end
    function feature_cell = gen_feat_cell(input, trialsNo_input)
           feature_array_tmp = cell2mat(input(1));
           feature_matrix_tmp = zeros(trialsNo_input(end), size(feature_array_tmp, 2));
           for clsfeat_cnt = 1:size(input, 2)
                feat_tmp = cell2mat(input(clsfeat_cnt));
                feat_tmp = shiftdim(feat_tmp);
                feature_matrix_tmp(sum(trialsNo_input(1:clsfeat_cnt))+1:sum(trialsNo_input(1:clsfeat_cnt+1)), :) = feat_tmp(:, :);
           end 
           feature_cell = mat2cell(feature_matrix_tmp, trialsNo_input(end), size(feature_array_tmp, 2));
    end

end

