l^%%
for i=1:15
    loaded_data = load("Data\subject_"+string(i)+".mat");
    data = loaded_data.data.train;
    data = dsample(data, 24);
    data = filterEEG(data);
    features = featureExtraction(data);
    feature_matrix = cell2mat(features.feature_matrix);
    
    pca_reduced_feature = zeros(size(feature_matrix, 1));
    [pca_trans_matrix, pca_reduced_feature] = pca(feature_matrix(:, 1:end-1));
    pca_reduced_feature = normalize(pca_reduced_feature);
    pca_reduced_feature = [pca_reduced_feature feature_matrix(:, end)];

    [javlue_reduced_feature, javlue_trans_matrix] = jvalue(features, 300);
    javlue_reduced_feature(:, 1:end-1) = normalize(javlue_reduced_feature(:, 1:end-1));
    %    
    test = loaded_data.data.test;
    test = mat2cell(test, size(test, 1), size(test, 2), size(test, 3));
    test = dsample(test, 24);
    test = filterEEG(test);
    test_feature = featureExtraction(test);
    test_feature = cell2mat(test_feature.feature_matrix);
    test_feature = test_feature(:, 1:end-1);
    test_feature_jvalue = test_feature(:, javlue_trans_matrix);
   
    mdl = fitcecoc(javlue_reduced_feature(:, 1:end-1), feature_matrix(:, size(feature_matrix, 2)));
    test_label = predict(mdl, test_feature_jvalue);
    ad = sprintf('subject_%d',i);
    result.(ad) = test_label;
end