function labels = createLabels(data)
    trialsNo = find_trialsNo(data);
    labels = zeros(1, trialsNo(6));
    for i = 1:length(trialsNo)-2
        labels(1, sum(trialsNo(1:i))+1:sum(trialsNo(1:i+1))) = i;  
    end

    function trial_numbers = find_trialsNo(input)
       trial_numbers = zeros(1, size(input, 2)+2);
       for length_cnt = 1:size(input, 2)
           array_tmp = cell2mat(input(length_cnt));
           trial_numbers(length_cnt+1) = size(array_tmp, 3);
       end
       trial_numbers(size(input, 2)+2) = sum(trial_numbers(1:size(input, 2)+1)); 
    end
end

